# PushyPushPush

This is a 'tool' written for the music production program [Renoise](https://renoise.com). It integrates the Ableton Push v1 control surface into the Renoise environment to allow hands on music composition via the external hardware.

### What can it do?

At present it's quite limited. So far it's only possible to move through the song, select patterns and tracks, enter and delete notes, alter the pattern length and change octave. Much more is planned and a framework to allow rapid extension of the capabilites has already been written.

### Controls

These controls are currently implemented, but will be subject to change as the project progresses:

| Push control | Renoise action |
| ------------ | -------------- |
| Tempo | Change pattern |
| Shift + Tempo | Move in pattern list |
| Swing/Up/Down | Move within pattern (+ Shift to move x10) |
| Encoder 3 | Change pattern length |
| Play | Start/stop playback |
| Shift + Play | Play from cursor |
| Record | Toggle edit mode |
| Selection controls/Left/Right | Select track |
| Octave Up/Down | Change octave |
| Pads | Add/delete notes (with edit mode active) |

### Installation

```
git clone https://cupcake99@bitbucket.org/cupcake99/pushypushpush.git
cd pushypushpush && ./pack_xrnx_PushyPushPush
```

Then drag and drop the packed xrnx file onto the Renoise window to install the tool.
