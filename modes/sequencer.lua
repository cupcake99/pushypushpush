local function sequencer (parent)
    local _state = parent._state
    local spec = {
        name = "sequencer",
        control = "note",
        page = {
            [1] = {
                lights = function ()
                    local lights = {
                        stop = Push.light.button.off,
                        note = Push.light.button.high,
                        x32t = Push.light.button.off,
                        x32 = Push.light.button.off,
                        x16t = Push.light.button.off,
                        x16 = Push.light.button.off,
                        x8t = Push.light.button.off,
                        x8 = Push.light.button.off,
                        x4t = Push.light.button.off,
                        x4 = Push.light.button.off,
                        softkey1A = Push.light.note_val.dim_yellow,
                        softkey2A = Push.light.note_val.dim_yellow,
                        softkey3A = Push.light.note_val.dim_yellow,
                        softkey4A = Push.light.note_val.dim_yellow,
                        softkey5A = Push.light.note_val.dim_yellow,
                        softkey6A = Push.light.note_val.dim_yellow,
                        softkey7A = Push.light.note_val.dim_yellow,
                        softkey8A = Push.light.note_val.dim_yellow,
                        softkey1B = Push.light.pad.light_grey,
                        softkey2B = Push.light.pad.light_grey,
                        softkey3B = Push.light.pad.light_grey,
                        softkey4B = Push.light.pad.light_grey,
                        softkey5B = Push.light.pad.light_grey,
                        softkey6B = Push.light.pad.light_grey,
                        softkey7B = Push.light.pad.light_grey,
                        softkey8B = Push.light.pad.light_grey,
                        mute = ((song.tracks[song.selected_track_index].mute_state ~= 1)
                            and Push.light.button.high + Push.light.blink.slow)
                            or Push.light.button.low,
                        csr_left = (song.selected_track_index == 1 and Push.light.button.off) or Push.light.button.low,
                        csr_right = (song.selected_track_index == (song.sequencer_track_count + song.send_track_count + 1)
                            and Push.light.button.off) or Push.light.button.low,
                        csr_up = (song.transport.edit_pos.line == 1 and Push.light.button.off) or Push.light.button.low,
                        csr_down = (song.transport.edit_pos.line == song.patterns[song.selected_pattern_index].number_of_lines
                            and Push.light.button.off) or Push.light.button.low,
                    }
                    return lights
                end,
                display = function ()
                    local display = table.copy(Push.display)
                    local z = 1
                    for i = _state.trackRange.from, _state.trackRange.to do
                        if i == _state.activeTrack then
                            display.line[1].zone[z] = ">" .. song.tracks[i].name
                        else
                            display.line[1].zone[z] = song.tracks[i].name
                        end
                        z = z + 1
                    end
                    display.line[2].zone[2] = (song.selected_instrument.name == "" and "un-named") or song.selected_instrument.name
                    display.line[2].zone[3] = " Length:"
                    display.line[3].zone[3] = "   " .. song.patterns[_state.activePattern].number_of_lines
                    display.line[4].zone[1] = "Page: " .. _state.activePage

                    return display
                end,
                action = function ()
                    local action = {}
                    action.tempo = function (...)
                        local data = select(2, ...)
                        if _state.shiftActive then
                            _state:changeSequence(data)
                        else
                            _state:changePattern(data)
                        end
                        _state:setPatternDisplay {0, 0, 1}
                    end
                    action.swing = function (...)
                        local data = select(2, ...)
                        if _state:setEditPos(data) then
                            _state:setPatternDisplay(data)
                        end
                    end
                    action.volume = _state.setMasterVolume
                    action.mute = function (...)
                        local data = select(2, ...)
                        local index = select(3, ...)
                        local muted = song.tracks[_state.activeTrack].mute_state ~= 1
                        if data[3] > 0 then
                            if muted then song.tracks[_state.activeTrack]:unmute()
                            else song.tracks[_state.activeTrack]:mute() end
                            _state.current[index].value =
                            ((song.tracks[_state.activeTrack].mute_state ~= 1)
                            and Push.light.button.high + Push.light.blink.slow) or Push.light.button.low
                        end
                    end
                    action.csr_up = action.swing
                    action.csr_down = action.swing
                    action.csr_left = function (...)
                        local data = select(2, ...)
                        _state:changeTrack(data)
                        _state:setPatternDisplay {0, 0, 1}
                    end
                    action.csr_right = action.csr_left
                    action.softkey1A = action.csr_left
                    action.softkey2A = action.csr_left
                    action.softkey3A = action.csr_left
                    action.softkey4A = action.csr_left
                    action.softkey5A = action.csr_left
                    action.softkey6A = action.csr_left
                    action.softkey7A = action.csr_left
                    action.softkey8A = action.csr_left
                    action.dial2 = _state.changeInstrument
                    action.oct_up = _state.changeOctave
                    action.oct_down = action.oct_up
                    action.dial3 = _state.changePatternLength

                    return action
                end
            },
            [2] = {
                lights = function ()
                    local lights = {
                        stop = Push.light.button.off,
                        note = Push.light.button.high,
                        x32t = Push.light.button.off,
                        x32 = Push.light.button.off,
                        x16t = Push.light.button.off,
                        x16 = Push.light.button.off,
                        x8t = Push.light.button.off,
                        x8 = Push.light.button.off,
                        x4t = Push.light.button.off,
                        x4 = Push.light.button.off,
                        softkey1A = Push.light.note_val.orange,
                        softkey2A = Push.light.note_val.orange,
                        softkey3A = Push.light.note_val.orange,
                        softkey4A = Push.light.note_val.orange,
                        softkey5A = Push.light.note_val.orange,
                        softkey6A = Push.light.note_val.orange,
                        softkey7A = Push.light.note_val.orange,
                        softkey8A = Push.light.note_val.orange,
                        softkey1B = Push.light.pad.light_grey,
                        softkey2B = Push.light.pad.light_grey,
                        softkey3B = Push.light.pad.light_grey,
                        softkey4B = Push.light.pad.light_grey,
                        softkey5B = Push.light.pad.light_grey,
                        softkey6B = Push.light.pad.light_grey,
                        softkey7B = Push.light.pad.light_grey,
                        softkey8B = Push.light.pad.light_grey,
                        mute = ((song.tracks[song.selected_track_index].mute_state ~= 1)
                            and Push.light.button.high + Push.light.blink.slow)
                            or Push.light.button.low,
                        csr_left = (song.selected_track_index == 1 and Push.light.button.off) or Push.light.button.low,
                        csr_right = (song.selected_track_index == (song.sequencer_track_count + song.send_track_count + 1)
                            and Push.light.button.off) or Push.light.button.low,
                        csr_up = (song.transport.edit_pos.line == 1 and Push.light.button.off) or Push.light.button.low,
                        csr_down = (song.transport.edit_pos.line == song.patterns[song.selected_pattern_index].number_of_lines
                            and Push.light.button.off) or Push.light.button.low,
                    }
                    return lights
                end,
                display = function ()
                    local display = table.copy(Push.display)
                    local z = 1
                    for i = _state.trackRange.from, _state.trackRange.to do
                        if i == _state.activeTrack then
                            display.line[1].zone[z] = ">" .. song.tracks[i].name
                        else
                            display.line[1].zone[z] = song.tracks[i].name
                        end
                        z = z + 1
                    end
                    display.line[2].zone[2] = (song.selected_instrument.name == "" and "un-named") or song.selected_instrument.name
                    display.line[2].zone[3] = " Length:"
                    display.line[3].zone[3] = "   " .. song.patterns[_state.activePattern].number_of_lines
                    display.line[4].zone[1] = "Page: " .. _state.activePage

                    return display
                end,
                action = function ()
                    local action = {}
                    action.tempo = function (...)
                        local data = select(2, ...)
                        if _state.shiftActive then
                            _state:changeSequence(data)
                        else
                            _state:changePattern(data)
                        end
                        _state:setPatternDisplay {0, 0, 1}
                    end
                    action.swing = function (...)
                        local data = select(2, ...)
                        if _state:setEditPos(data) then
                            _state:setPatternDisplay(data)
                        end
                    end
                    action.volume = _state.setMasterVolume
                    action.mute = function (...)
                        local data = select(2, ...)
                        local index = select(3, ...)
                        local muted = song.tracks[_state.activeTrack].mute_state ~= 1
                        if data[3] > 0 then
                            if muted then song.tracks[_state.activeTrack]:unmute()
                            else song.tracks[_state.activeTrack]:mute() end
                            _state.current[index].value =
                            ((song.tracks[_state.activeTrack].mute_state ~= 1)
                            and Push.light.button.high + Push.light.blink.slow) or Push.light.button.low
                        end
                    end
                    action.csr_up = action.swing
                    action.csr_down = action.swing
                    action.csr_left = function (...)
                        local data = select(2, ...)
                        _state:changeTrack(data)
                        _state:setPatternDisplay {0, 0, 1}
                    end
                    action.csr_right = action.csr_left
                    action.softkey1A = action.csr_left
                    action.softkey2A = action.csr_left
                    action.softkey3A = action.csr_left
                    action.softkey4A = action.csr_left
                    action.softkey5A = action.csr_left
                    action.softkey6A = action.csr_left
                    action.softkey7A = action.csr_left
                    action.softkey8A = action.csr_left
                    action.dial2 = _state.changeInstrument
                    action.oct_up = _state.changeOctave
                    action.oct_down = action.oct_up
                    action.dial3 = _state.changePatternLength

                    return action
                end
            }
        }
    }

    return spec
end

return sequencer
